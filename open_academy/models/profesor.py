# -*- coding: utf-8 -*-
from odoo import models, fields

class ProfesorContacto (models.Model) :
    _inherit = 'res.partner'
    x_titulado = fields.Boolean (string = 'Es titulado', default = False)
    x_antiguedad = fields.Integer (string = 'Antiguedad', default = 0)
    x_promedio = fields.Float (string = 'Promedio Evaluacion')