# -*- coding: utf-8 -*-
{
    'name': "Open Academy",
    'summary': """
    """,
    'description': """
        Modulo creado como practica
    """,
    'author': "Soluciones4G - German Barrientos",
    'website': "http://www.soluciones4g.com",
    # Categories can be used to filter modules in modules listing
    # for the full list
    'category': 'Uncategorized',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': [
        'base', 'contacts'
    ],

    # always loaded
    'data': [ 'views/res_partner_profesor_view.xml'
    ],
    'installable':True,
    'auto_install':False,
}
